window.addEventListener('DOMContentLoaded', function(){
  
  $(window).on("scroll", scrollEventHandler);
  $(document)
    .on("click", ".h-header__left-item, .h-header__nav--link", pageScroll)
    .on("click", ".sarch", search)
    .on("click", "#js-header__form", function(e) {
      e.stopPropagation();
    });

  function scrollEventHandler() {
    if ($(window).scrollTop() > 40) {
      $(".h-header").addClass("border-bottom");
    } else {
      $(".h-header").removeClass("border-bottom");
    }
  }

  function search() {
    if($(window).width() < 1024) {
      $(".h-header__right-sarch").toggleClass("open");
    }
  }

  function pageScroll(e) {
    e.preventDefault();
    const id = $(this).attr('href');
    const headerHeight = 80;

    if (id == "#top") {
      $("html, body").animate({ scrollTop: 0 }, 300, "swing");
    } else {
      $("html, body").animate({ scrollTop: parseInt($(id).offset().top) - headerHeight }, 300, "swing");
    }
  }

  // ==============================
  // slickの設定
  // ==============================
  $('#js-slick').slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    cssEase: 'ease-in-out',
    dots: true,
    fade: true,
    lazyLoad: 'progressive',
    slidesToShow: 1,
    speed: 1000
  });

});
